import urllib2

#from gpanel import *

def get_temperature(html):
    temp = ''
    
    return temp

def get_url(city=None, country=None):
    country = country or inputString("Pays : ")
    city = city or inputString("Nom de la ville : ")
        
    url = 'http://www.timeanddate.com/weather/{}/{}'.format(country, city)
    return url, country, city


def extract_string_from(string, to_find):
    sub_strings = to_find.split('...')
    start = 0
    temperature = 'Temperature not found'
    for s in sub_strings:
        if '{{capture}}' in s:
            (start_pattern, end_pattern) = s.split('{{capture}}')
            start = string.find(start_pattern, start) + len(start_pattern)
            end = string.find(end_pattern, start)
            temperature = string[start:end]
            return temperature.replace('&nbsp;Â', ' ')
        if s != '{{capture}}':
            start = string.find(s, start) + len(s)           
            
def main():
    # mécanisme permettant de redemander à l'utilisateur tant que la ville est introuvable
    while True:
        url, country, city = get_url()
        
        try:
            html = urllib2.urlopen(url).read()
            found = True
            
            to_find = 'Upcoming 5 hours...<tr class="h2 soft...<td>{{capture}}</td>'
            temperature = extract_string_from(html, to_find)
            
            print "La température à {}, {} est actuellement de {}".format(city, country, temperature)
        except urllib2.HTTPError:
            print "Impossible de trouver la ville", city, "en", country, " !!!"
    
        
        
main()