
# en Python 2, il faut mettre le u devant les chaines contenant des accents
last_name = 'Donner'
first_name = u'Cédric'
street = u'Rue de Corbières'
location = u'Bulle'
house_number = 46
zip_code = 1630

def address1():
    return first_name + ' ' + last_name + '\n' + str(house_number) + ', ' + street + '\n' + str(zip_code) + ', ' + location

def address2():
    # important de mettre le u devant la chaine pour pouvoir saisir les accents
    template = u'''
    {}, {}
    {}, {}
    {}, {}
    '''
    
    return template.format(first_name, last_name, house_number, street, zip_code, location)


print address1()

print address2()