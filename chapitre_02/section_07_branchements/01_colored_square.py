##### Importation des modules
from gturtle import *

##### Définition des fonctions

def square(side):
    if side < 50:
        setPenColor("red")
    else:
        setPenColor("green")
        
    repeat 4:
        forward(side)
        left(90)
        

def main():
    side = inputFloat("Veuillez saisir la longueur du côté du carré : ")
    square(side)
    

####### Programme principal        

makeTurtle()

main()