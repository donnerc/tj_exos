##### Importation des modules
from gturtle import *

##### Définition des fonctions

def step(length):
    forward(length)
    right(90)
    forward(length)
    left(90)

def figureA():
    LENGTH = 20
    count = 0
    repeat 10:
        if count > 5:
            setPenColor("blue")
        else:
            setPenColor("red")
        
        step(LENGTH)
        count += 1
        
def figureB():
    INCR = 5
    length = INCR
    count = 0
    repeat 62:
        if count < 22:
            setPenColor("green")
        elif count < 42:
            setPenColor("red")
        else:
            setPenColor("black")
        forward(length)
        right(90)
        length += 5
        count += 1
        
def main():
    figureB()
    
####### Programme principal        

makeTurtle()
hideTurtle()

main()