from gturtle import *

def onMousePressed(evt):
    penDown()
    x, y = evt.x, evt.y
    setScreenPos(Point(x, y))


def onMouseReleased(evt):
    penUp()

def onMouseDragged(evt):
    x, y = evt.x, evt.y
    
    # on peut faire print(dir(evt)) pour voir le contenu de l'objet avec
    # lequel la fonction onMouseDragged est appelée par le système
    # print(dir(evt))

    # Une fois qu'on connaît le contenu de `evt`, on peut deviner que les
    # attributs x et y représentent les coordonnées de la souris
    
    # les coordonnées de l'objet événement sont données dans le système de
    # coordonnées "screen coordinates" alors que les instructions de la tortue
    # telles que moveTo(x, y) utilisent des coordonnées tortues (turtle
    # coordinates) ... La fonction toTurtlePos(x_screen, y_screen) permet de
    # prendre des coordonnées écran et de les transformer en "turtle
    # coordinates"
    nextPos = toTurtlePos(Point(x, y))
    moveTo(nextPos)


makeTurtle(
    mousePressed=onMousePressed,
    mouseReleased=onMouseReleased,
    mouseDragged=onMouseDragged
)

speed(-1)