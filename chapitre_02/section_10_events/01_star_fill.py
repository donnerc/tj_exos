# -*- coding: utf-8 -*-

##### Importation des modules
from gturtle import *



def star(size):
    nb_tours = 4
    nb_sommets = 9
    angle = nb_tours * 360 / nb_sommets

    for _ in range(nb_sommets):
        forward(size)
        right(angle)


def onMouseHit(x, y):
    fill(x,y)
    
def onKeyPress(keyCode):
    


def main():
    star(300)    

####### Programme principal        

makeTurtle(mouseHit = onMouseHit, keyPress = onKeyPress)
setPos(0, -200)
speed(1000)
hideTurtle()

main()