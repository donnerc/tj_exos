##### Importation des modules
from gturtle import *

##### Définition des fonctions

def tree(s):
    if s == 0:
        return
        
    current_length = 5 * 2 ** s

    left(45)
    forward(current_length)
    tree(s - 1)
    back(current_length)
    right(90)
    forward(current_length)
    tree(s - 1)
    back(current_length)
    left(45)

def tronc():
    forward(150)

def main():
    tronc()
    tree(5)
    

####### Programme principal        

makeTurtle()
setPos(0, -200)
speed(1000)
hideTurtle()

main()