##### Importation des modules
from gturtle import *

##### Définition des fonctions

def tree_fractal(size):
    if size < 5:
        return

    # sauvegarde des coordonnées et de l'orientation actuelle pour
    # restauration ultérieure. On utilise dans cette ligne une affectation
    # parallèle permettant d'assigner directement une valeur aux deux
    # variables x et y.
    x, y = getX(), getY()
    orientation = heading()

    forward(size / 3)
    left(30)
    tree_fractal(2*size / 3)
    right(30)
    forward(size / 6)
    right(25)
    tree_fractal(size / 2)
    right(25)
    forward(size / 3)
    tree_fractal(size / 2)

    # restauration de la situation d'origine
    heading(orientation)
    setPos(x, y)

def main():
    tree_fractal(400)
    

####### Programme principal        

makeTurtle()
setPos(0, -200)
speed(1000)
hideTurtle()

main()