##### Importation des modules
from gturtle import *

##### Définition des fonctions

def star(size, n):
    if n < 1:
        return

    # on préfère ceci à repeat 6: qui ne se trouve pas dans
    # les versions standard de Python
    for _ in range(6):
        forward(size)
        star(size / 3, n -1)
        back(size)
        right(60)

def main():
    star(150, 3)
    

####### Programme principal        

makeTurtle()
setPos(0, 0)
speed(1000)
hideTurtle()

main()