##### Importation des modules
from gturtle import *

##### Définition des fonctions

def arc():
    repeat 90:
        forward(3)
        right(1)
        
def petal():
    setPenColor("red")
    setFillColor("red")
    startPath()
    arc()
    right(90)
    arc()
    right(90)
    fillPath()

def tige():
    setPenColor('black')
    penWidth(8)
    
    repeat 45:
        forward(5)
        right(1)
            
def flower():
    repeat 5:
        petal()
        left(72)
    right(135)
    tige()
    
####### Programme principal        

makeTurtle()

#speed(-1)
hideTurtle()
    
penWidth(2)    
flower()
