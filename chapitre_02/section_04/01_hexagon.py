from gturtle import *

t = makeTurtle()

hideTurtle()

def hexagon():
    side = 100
    n = 6
    repeat n:
        forward(side)
        right(60)
    
def figure():
    repeat 10:
        hexagon()
        right(36)
        
figure()
        