##### Importation des modules
from gturtle import *

##### Définition des fonctions

def square():
    right(45)
    startPath()
    repeat 6: 
        forward(100) 
        right(90)
    fillPath()
    left(225)
    
    
def version1():
    setFillColor("blue")
    setPenColor("blue")
    square()
    setPenColor("red")
    setFillColor("red")
    square()
    setPenColor("green")
    setFillColor("green")
    square()
    setPenColor("black")
    setFillColor("black")
    square()
    

def version2():
    colors = ['blue', 'red', 'green', 'black']
    for color in colors:
        setPenColor(color)
        setFillColor(color)
        square()
        
####### Programme principal        

makeTurtle()

speed(1000)
# hideTurtle()
    
setPos(-300,0)
penWidth(2)    
version1()