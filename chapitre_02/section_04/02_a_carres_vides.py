##### Importation des modules
from gturtle import *

##### Définition des fonctions

def square():
    right(45)
    repeat 6: 
        forward(100) 
        right(90)
    left(225)
    
def version1():
    setPenColor("blue")
    square()
    setPenColor("red")
    square()
    setPenColor("green")
    square()
    setPenColor("black")
    square()
    

def version2():
    colors = ['blue', 'red', 'green', 'black']
    for color in colors:
        setPenColor(color)
        square()
        
####### Programme principal        

t = makeTurtle()

hideTurtle()
    
setPos(-300,0)
penWidth(2)    
version1()