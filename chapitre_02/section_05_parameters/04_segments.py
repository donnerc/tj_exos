##### Importation des modules
from gturtle import *

##### Définition des fonctions

def segment(s, w):
   forward(s)
   right(w) 


def figure():
    repeat 92:
        segment(300, 151)
    

####### Programme principal        

makeTurtle()

setPos(-50, -100)
#speed(-1)
penWidth(1)
hideTurtle()

figure()
    

