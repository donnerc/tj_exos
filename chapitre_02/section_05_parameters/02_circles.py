##### Importation des modules
from gturtle import *

##### Définition des fonctions

def colorCircle(radius, color):
    setPenColor(color)
    rightArc(radius, 360)


def figure():
    colorCircle(30, "red")
    colorCircle(60, "green")
    colorCircle(90, "blue")
    colorCircle(120, "pink")
    

####### Programme principal        

makeTurtle()

#speed(-1)
penWidth(3)
hideTurtle()

figure()
    

