from gturtle import * 

'''

Explication du problème
=======================

Le paramètre formel `sidelength` de la fonction `pentagon` n'a pas été utilisé
dans la définition de la fonction. Autrement dit, on avance la tortue d'une
valeur codée en dur (hard coded) au lieu d'utiliser le paramètre `sidelength`

::

	forward(90)

'''

def pentagon(sidelength, color): 
   setPenColor(color) 
   repeat 5: 
      forward(sidelength) 
      left(72)

makeTurtle() 
pentagon(100, "red") 
left(120) 
pentagon(80, "green") 
left(120) 
pentagon(60, "violet")

