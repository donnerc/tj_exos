##### Importation des modules
from gturtle import *

##### Définition des fonctions


def figure1():
    repeat 37:
        forward(77)
        right(140.86)
        forward(310)
        right(112)
        
def figure2():
    repeat 46:
        forward(15.4)
        right(140.86)
        forward(62)
        right(112)
        forward(57.2)
        right(130)
        
        
def figure3():
    repeat 68:
        forward(31)
        right(141)
        forward(112)
        right(87.19)
        forward(115.2)
        right(130)
        forward(186)
        right(121.43)
    
figure = figure3  

####### Programme principal        

makeTurtle()

setPos(-50, -100)
#speed(-1)
penWidth(1)
hideTurtle()

figure()
    

