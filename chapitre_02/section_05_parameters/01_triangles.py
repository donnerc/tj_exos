##### Importation des modules
from gturtle import *

##### Définition des fonctions

def triangle(color):
    setPenColor(color)
    repeat 3:
        forward(100)
        left(120)

def figure():
    triangle("red")
    left(90)
    triangle("green")
    left(90)
    triangle("blue")
    left(90)
    triangle("pink")
    
def figure1():
    for color in ["red", "green", "blue", "pink"]:
        triangle(color)
        left(90)

####### Programme principal        

makeTurtle()

#speed(-1)
penWidth(3)
hideTurtle()

figure()
    

