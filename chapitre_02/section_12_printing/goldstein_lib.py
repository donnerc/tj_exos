# -*- coding: utf-8 -*-

'''

This library contains functions to draw Goldstein curves.

'''

from gturtle import *

def goldstein(n, params):
    '''
    
    ``n`` : nombre d'itérations à effectuer
    
    ``params`` est une liste de tuples de la forme (dist, angle) qui indique une étape de l'itération
    
    '''
    for _ in range(n):
        for (distance, angle) in params:
            forward(distance)
            right(angle)
            
def setup():
    ''' Setup the environment to draw Goldstein Curves (Creates Turtle etc ...) '''
    makeTurtle()
    speed(-1)
    hideTurtle()
    
def test():
    # mise en route de l'environnement
    setup()
    b
    # Exercice 1a : 31 itérations avec s = 300 et a = 151
    goldstein(n=31, params=[(300, 151)])
    
    # Exercice 1b : 142 itérations avec s = 400 et a = 159.72
    goldstein(n=142, params=[(400, 159.72)])
    
    # Exercice 2 : 37 itérations avec s = 77, a = 140.86 puis  s = 310 et a = 112
    goldstein(n=37, params=[(77, 140.86), (310, 112)])
    
    # Exercice 3 : 47 itérations avec s = 15.4, a = 140.86, puis s = 62 et a = 112 puis finalement s= 57.2 et a = 130
    goldstein(n=47, params=[(15.4, 140.86), (62, 112), (57.2, 130)])
    
if __name__ == '__main__':
    test()
    