from gturtle import *

def goldstein(n, distance, angle):
    for _ in range(n):
        forward(distance)
        right(angle)
    
def doIt():
    goldstein(142, 400, 159.72)

def main():
    makeTurtle()
    speed(-1)
    hideTurtle()
    
    setPos(0, -100) # screen
    doIt()
    setPos(-200, -100)      # printer
    printerPlot(doIt)
    
main()