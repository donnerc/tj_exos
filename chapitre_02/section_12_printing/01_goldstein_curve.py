from gturtle import *

def goldstein(n, distance, angle):
    for _ in range(n):
        forward(distance)
        right(angle)
    
def doIt():
    goldstein(31, 300, 151)

def main():
    makeTurtle()
    speed(-1)
    hideTurtle()
    
    setPos(0, -100) # screen
    doIt()
    setPos(-250, -100)      # printer
    printerPlot(doIt)
    
main()