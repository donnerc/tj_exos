from gturtle import *

from goldstein_lib import goldstein, setup
    
def doIt():
    goldstein(n=47, params=[(15.4, 140.86), (62, 112), (57.2, 130)])

def main():
    setup()
    
    setPos(200, -100) # screen
    doIt()
    setPos(0, -100)      # printer
    printerPlot(doIt)
    
if __name__ == '__main__':
    main()