from gturtle import *

def goldstein(n, params):
    '''
    
    ``params`` est une liste de tuples de la forme (dist, angle) qui indique une étape de l'itération
    
    '''
    for _ in range(n):
        for (distance, angle) in params:
            forward(distance)
            right(angle)
    
def doIt():
    goldstein(142, [(77, 140.86), (310, 112)])

def main():
    makeTurtle()
    speed(-1)
    hideTurtle()
    
    setPos(0, -100) # screen
    doIt()
    setPos(-200, -100)      # printer
    printerPlot(doIt)
    
main()