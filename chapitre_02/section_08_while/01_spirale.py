##### Importation des modules
from gturtle import *

##### Définition des fonctions

def spirale():
    length = 5

    while length < 150:
        forward(length)
        right(70)
        length += 0.5
        

def main():
    spirale()
    

####### Programme principal        

makeTurtle()
speed(-1)
hideTurtle()

main()