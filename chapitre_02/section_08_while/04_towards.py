##### Importation des modules
from gturtle import *

##### Définition des fonctions

## Il n'est pas pertinent d'utiliser une boucle while dans cet exercice,
# même s'il est dans le chapitre des boucles while


def main():
    # représenter le cible au centre
    dot(10)
    
    # position initiale


####### Programme principal        

    x, y = 250, 200
    setPos(x, y)
    
    # régler l'orientation
    angle = towards(0, 0)
    heading(angle)

    while distance(0, 0) >= 1:
        forward(10)
        

makeTurtle()
speed(150)
# hideTurtle()

main()