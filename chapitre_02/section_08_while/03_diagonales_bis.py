##### Importation des modules
from gturtle import *

##### Définition des fonctions

## Il n'est pas pertinent d'utiliser une boucle while dans cet exercice,
# même s'il est dans le chapitre des boucles while
def diag_up():
    x, y = -200, 200

    while x < 200:
        moveTo(x, y)
        dot(25)
        x += 40
        y -= 40
        
def diag_to(x, y, n, color):
    moveTo(x, y)
    step = distance(0,0) / n
    delta_x, delta_y = x / n, y / n
    
    while distance(0,0) > step / 2:
        setPenColor("blue")
        moveTo(x, y)
        setPenColor(color)
        dot(25)
        x -= delta_x
        y -= delta_y


def main():
    size = 200
    n_dots = 5

    diag_to(-size, size, n_dots, "red")
    diag_to(size, size, n_dots, "blue")
    diag_to(-size, -size, n_dots, "green")
    diag_to(size, -size, n_dots, "pink")
    diag_to(size, 0, n_dots - 1, "cyan")
    diag_to(-size, 0, n_dots - 1, "black")
    diag_to(0, size, n_dots - 1, "orange")
    diag_to(0, -size, n_dots - 1, "violet")
    

####### Programme principal        

makeTurtle()
speed(-1)
# hideTurtle()

main()