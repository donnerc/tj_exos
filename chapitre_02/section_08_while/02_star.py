##### Importation des modules
from gturtle import *

##### Définition des fonctions

## Il n'est pas pertinent d'utiliser une boucle while dans cet exercice,
# même s'il est dans le chapitre des boucles while
def star():
    angle = 142
    repeat 50:
        forward(200)
        left(angle)
        
def main():
    star()
    

####### Programme principal        

makeTurtle()
speed(-1)
hideTurtle()

main()