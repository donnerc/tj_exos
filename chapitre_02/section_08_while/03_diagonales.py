##### Importation des modules
from gturtle import *

##### Définition des fonctions
        
def diag_to(x, y, n):
    moveTo(x, y)
    step = distance(0,0) / n
    delta_x, delta_y = x / n, y / n
    
    while distance(0,0) > step / 2:
        setPenColor("blue")
        moveTo(x, y)
        setPenColor("red")
        dot(25)
        x -= delta_x
        y -= delta_y


def main():
    size = 250
    n_dots = 8

    diag_to(-size, size, n_dots)
    diag_to(size, size, n_dots)
    diag_to(-size, -size, n_dots)
    diag_to(size, -size, n_dots)
    diag_to(size, 0, n_dots)
    

####### Programme principal        

makeTurtle()
speed(-1)
# hideTurtle()

main()