##### Importation des modules
from gturtle import *

##### Définition des fonctions

## Il n'est pas pertinent d'utiliser une boucle while dans cet exercice,
# même s'il est dans le chapitre des boucles while


def main():
    # représenter le cible au centre
    dot(10)
    
    # position initiale


####### Programme principal        

    x, y = 250, 200
    setPos(x, y)
    
    # régler l'orientation
    angle = towards(0, 0)
    heading(angle)

    #####################################################################
    # Boucle while avec condition problématique
    #####################################################################
    # avec cette condition qui vise à arrêter la tortue de manière plus
    # précise, il est pratiquement impossible qu'elle s'arrête sur la cible.
    # La raison est qu'il y a de fortes chances, en avancçant par pas de 10,
    # que la tortue "saute par-dessus la cible" et se trouve au-delà de la
    # cible à une distance trop grande pour que la condition soit satisfaite.
    # Ce genre de problème survient souvent lorsqu'on ne réfléchit pas bien à
    # la condition d'une boucle while.
    #
    # pour régler le problème, on peut réduire la grandeur des étapes faites à
    # chaque itération de la boucle
    while distance(0, 0) >= 1 / 10:
        forward(10)
        

makeTurtle()
speed(150)
# hideTurtle()

main()