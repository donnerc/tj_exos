from gturtle import *

s = 200
n = 6

makeTurtle()
setPos(-s/2, -s/2)

def drawLine(a, b):
    ax = a.getX()
    ay = a.getY()
    ah = a.heading()
    a.moveTo(b.getX(), b.getY())
    a.setPos(ax, ay)
    a.heading(ah)

def makeClone():
    turtle = clone()
    turtle.speed(-1)
    forward(s)
    right(360 / n)
    return turtle
    
# generate Turtle clone
turtles = [makeClone() for _ in range(n)]
hideTurtle()

while True:

    for i in range(n):
        turtles[i].setHeading(turtles[i].towards(turtles[(i+1) % n]))
    for i in range(n):
        drawLine(turtles[i], turtles[(i+1) % n])
    for i in range(n):
        turtles[i].forward(5)
