from gturtle import *


def onKeyPressed(code):
    print(code)
    

def motherStep(step, angle):
    mother.forward(step)
    mother.left(angle)
    
def childStep(step):
    direction = child.towards(mother)
    child.heading(direction)
    child.forward(step)
    

turtleFrame = TurtleFrame()

mother = Turtle(turtleFrame, "green",
    keyPressed=onKeyPressed)
child = Turtle(turtleFrame, "red")

## choisir une position aléatoire pour child
child.setRandomPos(400, 400)
mother.speed(-1)
    
finish = False
while not finish:
    motherStep(4, 2)
    childStep(4)
    

    
    