from gturtle import *

turtleFrame = TurtleFrame()

def draw_stars(n, size, turtles):
    for _ in range(n):
        for t in turtles:
            t.forward(size)
            t.right(2 * 360/n)
            
turtles = [Turtle(turtleFrame, c) for c in ["cyan", "red", "green"]]

start_positions = [
    (-150, 0),
    (0,0),
    (150, 0),
]

for i, pos in enumerate(start_positions):
    turtles[i].setPos(pos[0], pos[1])
    turtles[i].speed(-1)

draw_stars(5, 140, turtles)