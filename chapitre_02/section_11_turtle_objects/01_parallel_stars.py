from gturtle import *

turtleFrame = TurtleFrame()

def draw_stars(n, size, turtles):
    for _ in range(n):
        for t in turtles:
            t.forward(size)
            t.right(2 * 360/n)

t1 = Turtle(turtleFrame, "cyan")
t2 = Turtle(turtleFrame, "red")
t3 = Turtle(turtleFrame, "green")

t1.setPos(-150, 0)
t2.setPos(0,0)
t3.setPos(150, 0)

draw_stars(5, 140, [t1, t2, t3])