##### Importation des modules
from gturtle import *

##### Définition des fonctions

def segments(angle):
    repeat 30:
        forward(100)
        left(angle)

def main():
    angle = inputFloat("Veuillez saisir un angle")  
    segments(angle)

####### Programme principal        

makeTurtle()

#speed(-1)
penWidth(1)
hideTurtle()

main()
    

