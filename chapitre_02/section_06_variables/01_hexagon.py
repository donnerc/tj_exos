##### Importation des modules
from gturtle import *

##### Définition des fonctions

def hexagon(n):
    angle = 360 / n
    repeat n:
        forward(100)
        left(angle)

def main():
    n = inputInt("Veuillez saisir le nombre de côtés")
    hexagon(n)

####### Programme principal        

makeTurtle()

penWidth(3)
setPos(200, 0)
hideTurtle()

main()