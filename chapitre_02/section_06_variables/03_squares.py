##### Importation des modules
from gturtle import *

##### Définition des fonctions

def square(side_length):
    repeat 4:
        forward(side_length)
        right(90)

def main():
    side_length = 8
    repeat 10:
        square(side_length)
        side_length += 10

####### Programme principal        

makeTurtle()

#speed(-1)
penWidth(2)
hideTurtle()

main()
    

