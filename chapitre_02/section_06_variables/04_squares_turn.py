##### Importation des modules
from gturtle import *

##### Définition des fonctions

def square(side_length):
    repeat 4:
        forward(side_length)
        right(90)

def main():
    side_length = inputInt("Côté du plus grand carré")
    repeat 20:
        square(side_length)
        side_length *= 0.9
        left(10)

####### Programme principal        

makeTurtle()

#speed(-1)
penWidth(2)
hideTurtle()

main()
    

