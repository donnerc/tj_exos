from gpanel import *


makeGPanel(0, 100, 0, 100)

def symmetry(points, vertical_axis):
    sym_points = []
    for i, p in enumerate(points):
        x, y = p
        sym_points.append( [x + 2 * (vertical_axis - x), y] )
        
    return sym_points
        

def drawHeart():
    points = [
        [40, 0],
        [-20, 70],
        [40, 80],
        [40, 50],
    ]
    
    for p in points:
        move(p)
        fillCircle(1)
    
    
    # draw Bezier curve
    setColor("red")
    lineWidth(3)
    # cubicBezier(points[0], points[1], points[2], points[3])
    cubicBezier(*points)
    cubicBezier(*symmetry(points, 40))
    
    
drawHeart()