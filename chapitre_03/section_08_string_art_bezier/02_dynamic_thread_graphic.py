from gpanel import *

makeGPanel(0, 100, 0, 100)
     
pA = [10, 10]
pB = [90, 20]
pC = [30, 90]

def updateGraphics():
    clear()
    line(pA, pB)
    line(pA, pC)
    line(pB, pC)
    
    r = 0
    while r <= 1:
        pX1 = getDividingPoint(pA, pB, r)
        pX2 = getDividingPoint(pA, pC, 1 - r)
        pX3 = getDividingPoint(pB, pC, r)
        line(pX1, pX2)
        line(pX1, pX3)
        r += 0.02

def myCallback(x, y):
    pC[0] = x
    pC[1] = y
    updateGraphics()

makeGPanel(0, 100, 0, 100, 
              mousePressed = myCallback,
              mouseDragged = myCallback)

pA = [10, 10]
pB = [90, 20]
pC = [30, 90]
updateGraphics()

