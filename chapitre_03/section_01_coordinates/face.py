from gpanel import *

makeGPanel(0, 20, 0, 20)

def vectorAdd(v1, v2):
    return [v1[0] + v2[0], v1[1] + v2[1]]

center = [10, 10]

def face():
    setColor("black")
    move(center)
    circle(8)
    
    eye(vectorAdd(center, [-2, 1]))
    eye(vectorAdd(center, [2, 1]))
    
    nose()
    move(3,3)
    mouth()
    
def eye(coords, color="blue"):
    move(coords)
    setColor(color)
    fillCircle(1)
    
def nose(top_coords, length):
    move(top_coords)
    # continuer ici ...
    
def mouth():
    pass

face()
