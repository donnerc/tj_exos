from gpanel import *

makeGPanel(0, 600, 0, 600)


def triangle(x, y, r):
    fillTriangle(
        x - math.sqrt(3)/2 * r, y - r/2,
        x + math.sqrt(3)/2 * r, y - r/2,
        x, y + r
    )


def star(x, y, r, color="black"):
    setColor(color)
    triangle(x, y, r)
    triangle(x, y, -r)
    

def randomDifferentColor(exclude="gray"):
    color_choice = None
    while not color_choice:
        choice = getRandomX11Color()

        # ne fonctionne pas parfaitement ... certaines étoiles ne sont pas
        # visibles ... Comme si elles avaient tout de même la couleur de fond.
        # Peut-être une couleur très proche de celle qui est utilisée pour le
        # fond ...
        if choice != exclude:
            color_choice = choice
            
    return color_choice
        
 
def mosaic(size):
    # expliquer cette ligne assez cabalistique
    step_x, step_y = (size * 2 + 5, ) * 2
    for y in range(size + 5, 600, step_y):
        for x in range(size + 5, 600, step_x):
            color = randomDifferentColor("gray")
            star(x, y, size, color)


def main():
    bgColor("gray")
#    star(200, 200, 50, "red")
    mosaic(10)

if __name__ == '__main__':
    main()