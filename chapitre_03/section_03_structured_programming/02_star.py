from gpanel import *

makeGPanel(0, 400, 0, 400)


def triangle(x, y, r):
    fillTriangle(
        x - math.sqrt(3)/2 * r, y - r/2,
        x + math.sqrt(3)/2 * r, y - r/2,
        x, y + r
    )

def star(x, y, r):
    triangle(x, y, r)
    triangle(x, y, -r)


def main():
    star(200, 200, 50)

if __name__ == '__main__':
    main()