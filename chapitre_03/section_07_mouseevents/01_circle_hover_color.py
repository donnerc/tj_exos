from gpanel import *

# Paramètres du programme sous forme de variables globales
hoverColor = "red"
color = "green"
# Les points sont généralement représentés par des listes de deux nombres dans TigerJython
center = [0,0]
radius = 6


def inCircle(x, y):
    # Mettre directement l'expression booléenne dans la valeur de retour
    # La gauche de l'inégalité correspond à Pythagore
    return (x**2 + y**2) ** 0.5 <= radius

def onMouseMoved(x, y):
    if inCircle(x,y):
        fill(center, color, hoverColor)
    else:
        fill(center, hoverColor, color)

makeGPanel(-10, 10, -10, 10,
    mouseMoved = onMouseMoved
)
setColor(color)   
move(center)
fillCircle(radius)