from gpanel import *

color = "red"

isEmpty = True

def onMouseClicked(x, y):
    # fanion permettant de gérer le dessin du premier point
    global isEmpty
    
    if isEmpty:
        move(x,y)
        isEmpty = False
    else:
        draw(x,y)
    


makeGPanel(-10, 10, -10, 10,
    mouseClicked = onMouseClicked
)

setColor(color)   