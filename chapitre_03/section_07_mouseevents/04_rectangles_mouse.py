# Auteur : Cédric Donner
# TigerJython : chapitre 03 / 07 mouse events / 04 : mouse dynamic rectangles

'''

But de cet exercice
===================

*  Montrer comment reprendre un code existant qui implémente une fonctionnalité 
   similaire et repérer les changements minimuax à opérer.
   
*  Tester le programme avec les cas pathologiques
  (cliquer deux fois au même endroit sans bouger ...)
   ne devrait rien dessiner, mais dessine un rectangle depuis
   l'ancienne position ...
   
Bug connu
---------

Lorsque l'utilisateur ne glisse pas la souris mais ne fait que de la déplacer,
il y a un comportement inatendu est bizarre...

    *  Devoir supplémentaire facultatif : corriger ce bug !
   
'''


from gpanel import *

tempColor = "white"
color = "green"

def onMousePressed(x, y):
    global x1, y1
    storeGraphics()
    x1 = x
    y1 = y
    x2 = x1
    y2 = y1
    setColor(tempColor)
 
def onMouseDragged(x, y):
    global x2, y2
    recallGraphics()
    x2 = x
    y2 = y
    rectangle(x1, y1, x2, y2)

def onMouseReleased(x, y):
    setColor(color)
    if not (x1 == x2 and y1 == y2):
        rectangle(x1, y1, x2, y2) 

# montrer comment formater comme il faut ce genre de code avec beaucoup de paramètres
makeGPanel(-10, 10, -10, 10,
    mousePressed = onMousePressed, 
    mouseDragged = onMouseDragged,
    mouseReleased = onMouseReleased
)
              
title("Temporary Rectangles")
bgColor("black")
setColor(color)
lineWidth(2)