from gpanel import *

# paramètres de configuration du programme
color = "red"
r_min = 0.01
r_max = 0.1
r = r_min
delta_r = 0.005


def drawRedBlackCircle(r):
    fillCircle(r)
    setColor("black")
    circle(r)
    setColor(color)


def onMouseMoved(x, y):
    global r
    
    # déplacer le curseur de dessin à la position actuelle de la souris
    move(x,y)
    
    # réinitialiser périodiquement le rayon à r_min
    if r > r_max:
        r = r_min
        
    drawRedBlackCircle(r)
    
    r += delta_r

# Ne pas mettre des coordonnées trop grandes, sinon on ne voit rien ...
makeGPanel(-1, 1, -1, 1,
    mouseMoved = onMouseMoved
)

setColor(color)   