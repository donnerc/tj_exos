import math
from gpanel import *

def step():
    global t
    x = r * math.cos(omega_x * t)
    y = r * math.sin(omega_y * t)
    t = t + 0.001
    if t > 6.28:
        t = 0
        setColor(getRandomX11Color())
    move(x, y)
    fillCircle(4)

makeGPanel(-500, 500, -500, 500)
bgColor("darkgreen")

t = 0
r = 200 

# cf page Wikipedia des courbes de Lissajous : 
# https://fr.wikipedia.org/wiki/Courbe_de_Lissajous
omega_x, omega_y = 5, 7

while True:
    step()
    # Pour accélérer l'animation, diminuer le délai. Minimum = 1 milliseconde.
    delay(1)

