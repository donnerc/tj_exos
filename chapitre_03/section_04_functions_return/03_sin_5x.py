from math import sin, cos, pi

from plot import plot, setup


def f1(x):
    return sin(5*x)

def f2(x):
    return sin(2*x)


def main():
    setup(0, 2*pi, -1, 1)
    plot(f1, 0, 2 * pi, color="blue")
    # on pourrait utiliser également une expression lambda
    # plot(lambda x: sin(5*x), 0, 2 * pi, color="blue")
    plot(f2, 0, 2 * pi, color="red")

if __name__ == '__main__':
    main()