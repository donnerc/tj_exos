from math import sin, cos, pi

from plot import plot, setup


def f(x):
    return 1 / sin(x)


def main():
    xmin, xmax = -5, 5
    ymin, ymax = -100, 100
    setup(xmin, xmax, ymin, ymax)
    plot(f, xmin, xmax, n=10000)


if __name__ == '__main__':
    main()