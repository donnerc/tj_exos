from gpanel import *

def setup(xmin, xmax, ymin, ymax):
    width = xmax - xmin
    height = ymax - ymin
    makeGPanel(xmin - width * 0.1, xmax + width * 0.1, ymin - height * 0.1, ymax + height * 0.1)
    setColor("gray")
    drawGrid(xmin, xmax, ymin, ymax)


def plot(f, xmin=-5, xmax=5, n=1000, color="blue", width=2):
    setColor(color)
    lineWidth(width)
    step = (xmax - xmin) / n
    x  = xmin
    while x < xmax:
        y = f(x)
        if x == xmin:
            move(x, y)
        else:
            draw(x, y)
        x = x + step