
def average(a, b):
	return (a + b) / 2

def assertAlmostEqual(real, should_be, delta=0.00005):
	should_be = float(should_be)
	passed = should_be - delta < real < should_be + delta

	if passed:
		print("real value :", real, "should_be be", should_be, "test passed")
	else:
		print("real value :", real, "should be", should_be, "test failed")


def test():
	assertAlmostEqual(average(0, 10), should_be=5)
	assertAlmostEqual(average(5, 5), should_be=5)
	assertAlmostEqual(average(1, 4), should_be=2.5)
	assertAlmostEqual(average(-5, -1), should_be=-3)
	assertAlmostEqual(average(2/3, 0), should_be=1/3)

if __name__ == '__main__':
	test()