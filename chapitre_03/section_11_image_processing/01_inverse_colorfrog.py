from gpanel import *

size = 300

makeGPanel(Size(2 * size, size))
window(0, 2 * size, size, 0)    # y axis downwards
img = getImage("sprites/colorfrog.png")
w = img.getWidth()
h = img.getHeight()
image(img, 0, size)
for x in range(w):
    for y in range(h):
        color = img.getPixelColor(x, y)
        red = color.getRed()
        green = color.getGreen()
        blue = color.getBlue()
        
        newColor = makeColor(green, red, blue)
        img.setPixelColor(x, y, newColor)
image(img, size, size)

