from gpanel import *

from math import atan2

size = 500

def rotateImg(img, angle):
    rotatedImg = GBitmap.scale(img, 1, angle)
    image(rotatedImg, 0, size)
    
def rotationAngle(x, y, xcenter=0, ycenter=0):
    return math.degrees(math.atan2(ycenter-y, xcenter-x))
    

def onMouseClick(x, y):
    clear()
    print "clicked at", (x, y)
    angle = rotationAngle(x, y,  220, 320)
    print "rotation angle", str(angle) + "°"
    rotateImg(img, angle)

makeGPanel(Size(size, size), mouseDragged=onMouseClick)
window(0, size, size, 0)    # y axis downwards
img = getImage("sprites/colorfrog.png")
width = img.getWidth()
height = img.getHeight()
image(img, 0, size)


