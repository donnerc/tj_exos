# Originaly written by Maxime Jan
# https://bitbucket.org/maxime_jan/tj_exos/raw/2bd0cdf77bdcc1d57f8127a2b530cad0780142f0/chapitre_03/section_11/exo_03.py

from gpanel import *
from math import *

def onMousePressed(x, y):
    global center
    center = [x,y]
    move(center)
    storeGraphics()
    setColor(makeColor(getPixelColor(x, y)))
 
def onMouseDragged(x, y):
    recallGraphics()
    fillCircle(sqrt((x - center[0])**2 + (y - center[1])**2))

size = 300
makeGPanel(
    Size(size, size),
    mousePressed = onMousePressed, 
    mouseDragged = onMouseDragged
)

window(0, size, size, 0)    # y axis downwards
img = getImage("sprites/colorfrog.png")
image(img, 0, size)