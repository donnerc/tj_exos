from random import randint
from time import sleep

from gpanel import *


def draw_balls(n=10):
    for y in [0.5, 9.5]:
        for x in [i + 0.5 for i in range(10)]:
            move(x, y)
            fillCircle(0.5)


def sample_n(min, max, n):
    sample = [0] * n

    for i in range(n):
        sample[i] = randint(min, max)
        
    return sample

def draw_experience(sample=None, pause=0.5):
    draw_balls(NB_BALLS)
    lineWidth(3)
    for i in range(len(sample)):
        
        x1 = i + 0.5
        y1 = 0.5
        x2 = sample[i] + 0.5   
        y2 = 9.5
        line(x1, y1, x2, y2)
        
    sleep(pause)
    clear()


def do_experience(n, time_step=0.5):
    assert n > 0
    
    total_hits = 0
    
    for i in range(n):
        sample = sample_n(min=0, max=9, n=10)
        draw_experience(sample, pause=time_step)
        
        hits = len(set(sample))
            
        nb_turns = i + 1
        total_hits += hits
        avg_hits = total_hits / nb_turns
        
        message = "Nb turns : {nb_turns} | nb_hits : {total_hits} | average hits : {avg_hits}".format(
            nb_turns=nb_turns,
            total_hits=total_hits,
            avg_hits=avg_hits
        )
        
        title(message)
        
        print(message)
        print("nb_hits", hits)

        

NB_BALLS = 10
MAX_RUNS = 100

panel = makeGPanel(0, NB_BALLS+1 * 1.1, 0, 10 * 1.1)
do_experience(MAX_RUNS, time_step=0.5)
    
