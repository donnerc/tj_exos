

from gpanel import *
import random

NB_DROPS = 10000

def onMousePressed(x, y):
    if isLeftMouseButton():
        move(x, y)
    if isRightMouseButton():
        '''
        On omet de tester si le point (x,y) se situe à l'intérieur 
        ou à l'extérieur de la surface fermée tracée. On ne vérifie
        d'ailleurs pas non plus si la surface est effectivement fermée.
        
        Une solution pourrait être d'approximer le forme tracée par 
        un polynôme contenant de multiples sommets et d'utiliser
        ensuite fillPolygon() comme dans le programme original
        '''
        fill(x,y, "black", "gray")
        wakeUp()
        
    

def onMouseDragged(x, y):
    draw(x, y)

def go():
    global nbHit
    title("Working. Please wait...")
    for i in range(NB_DROPS):
        pt = [100 * random.random(), 100 * random.random()]
        color = getPixelColorStr(pt)
        if color == "black":
            setColor("green")
            point(pt)
        if color == "gray" or color == "red":
            nbHit += 1
            setColor("red")
            point(pt)
    title("All done. #hits: " + str(nbHit) + " of " + str(NB_DROPS))

makeGPanel(0, 100, 0, 100,
    mousePressed=onMousePressed,
    mouseDragged=onMouseDragged
)
title("Select corners with left button. Start dropping with right button")
bgColor("black")
setColor("white")
corners = []
nbHit = 0
putSleep()
go()

