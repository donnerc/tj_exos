from gpanel import *
import random

MAXITERATIONS = 1000000
makeGPanel(-10, 10, -10, 10)

pA = [0, -10]
pB = [10, 0]
pC = [0, 10]
pD = [-10, 0]

#polygon(pA, pB, pC, pD)
corners = [pA, pB, pC, pD]
polygon(corners)

# quelle est l'importance de ce point? Peut-on on le choisir de manière arbitraire ?
pt = [0, 0]

title("Working...")
for iter in range(MAXITERATIONS):
    i = random.randint(0, 3)
    pRand = corners[i]
    pt = getDividingPoint(pRand, pt, 0.45)
    point(pt)
title("Working...Done")