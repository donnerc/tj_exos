from random import randint
from time import sleep

from gpanel import *

def onMouseMove(x, y):
    # print x, y
    pass

panel = makeGPanel(0, 12, 0, 10, mouseMoved = onMouseMove)

MAX_RUNS = 100

def sample_n(min, max, n):
    sample = [0] * n

    for i in range(n):
        sample[i] = randint(min, max)
        
    return sample

def draw_experience(sample=None):
    def draw_circles(n, x):
        y = 0.5
        for i in range(n):
            move(x, y + i)
            fillCircle(.5)
            
    max_children = 1
        
        
    setColor("black")
    for month in range(1, 13):
        x = month - 0.5
        # attention, le curseur doit se trouver au milieu du rectangle
        move(x, 5)
        rectangle(1, 10)
        
        nb_children = sample.count(month)
        max_children = max(max_children, nb_children)
        draw_circles(nb_children, x)
        
    sleep(.1)
    clear()   
    
    return max_children 

def do_experience(n):
    assert n > 0
    
    nb_positive = 0
    proba = None
    
    for i in range(n):
        sample = sample_n(1, 12, 5)
        nb_children = draw_experience(sample)
        
        if nb_children > 1:
            nb_positive += 1
            
        total = i + 1
        proba = nb_positive / total
        title("Nb exp : {nb_positive} | total : {total} | probability : {proba}".format(
            nb_positive=nb_positive,
            total=total,
            proba=proba
        ))
        print "Experience", total, "Birth month :", sample
            
        
        
do_experience(MAX_RUNS)
    
