from __future__ import print_function

from gpanel import *
from javax.swing import *


def makeActionHandler(color):
    '''
    
    This function uses a closure to make create a special actionHandler from the color parameter. 
    This function returns A FUNCTION (closure)
    
    '''
    def colorHandler(e):
        setColor(color)
    return colorHandler

def makeGUI():
    def onGo(e):
        print("Go !")
        wakeUp()
        
    menuBar = JMenuBar()
    
    # Menu Options
    optionsMenu = JMenu("Options")
    for label, color in [("Rouge", "red"), ("Vert", "green"), ("Bleu", "blue")]:
        menuItem = JMenuItem(color, actionPerformed=makeActionHandler(color))
        optionsMenu.add(menuItem)
    menuBar.add(optionsMenu)
    
    # Menu Go
    goItem = JMenuItem("Go", actionPerformed=onGo)
    menuBar.add(goItem)
    
    makeGPanel(menuBar, 0, 100, 0, 100)
    
    
def doIt():
    clear()
    line(pA, pB)
    line(pA, pC)
    
    r = 0
    while r <= 1:
        pX1 = getDividingPoint(pA, pB, r)
        pX2 = getDividingPoint(pA, pC, 1 - r)
        line(pX1, pX2)
        r += 0.05
        delay(300)
    
pA = [10, 10]
pB = [90, 20]
pC = [30, 90]


makeGUI()
while not isDisposed():
    putSleep()
    doIt()