# TigerJython
# Exercise 3.13.1 : Moiré UI

from gpanel import *
from javax.swing import JLabel, JTextField, JButton

makeGPanel(0, 10, 0, 10)

def onOk(e):
    global interrupt
    interrupt = True
    wakeUp()

def buildGUI(components):
    for c in components:
        addComponent(c)
    validate()


def doIt(latency):
    clear()
    for i in range(11):
        for k in range (11):
            line(i, 0, k, 10)
            delay(latency)
            
    for i in range(11):
        for k in range (11):
            line(0, i, 10, k)
            delay(latency)


interrupt = False
latencyLbl = JLabel("Temps de latence: ")
latencyTextField = JTextField(4)
okBtn = JButton("OK", actionListener = onOk)

buildGUI([
    latencyLbl,
    latencyTextField,
    okBtn
])

while True:
    putSleep()
    latency = int(latencyTextField.getText())
    doIt(latency)
    

